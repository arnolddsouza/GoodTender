*how to run the project
<1> Download and extract the zip file
<2> Create Project into your system with name as goodtender
<3> Replace your project src folder with the downloaded one.
<4> Run the Project with command ng serve --open

*Software Requirements
<1> Node.js
<2> Angular CLI
<3> Visual Studio Code

NOTE: Refer angular.io website for installation and configuration
